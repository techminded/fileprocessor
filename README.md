# Fileprocessor

Fileprocessor scans text files for patterns that are loaded from files to match and clean. Written to clean files from malware.

## To build fat standalone jar

```
#!bash
sbt assembly

```


## The run

```
#!bash
java -jar fileprocessor-assembly-0.1.jar -a replace -f /path/to/scan -p pattern.txt -p pattern2.txt -x php

```