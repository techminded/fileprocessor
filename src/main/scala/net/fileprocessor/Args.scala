package net.fileprocessor

import com.beust.jcommander.Parameter
import collection.JavaConversions._


object Args {
  @Parameter(
    names = Array("-f", "--files"),
    description = "Files to load. Can be specified one or multiple times.")
  var files: java.util.List[String] = null
  @Parameter(
    names = Array("-a", "--actions"),
    description = "Actions to perform")
  var actions: java.util.List[String] = null
  @Parameter(
    names = Array("-p", "--pattern"),
    description = "Patterns to match, can be path to file to read pattern from")
  var pattern: java.util.List[String] = null
  @Parameter(
    names = Array("-x", "--file-extensions"),
    description = "Patterns to match, can be path to file to read pattern from")
  var extensions: String = null
  @Parameter(
    names = Array("-m", "--replacement-mode"),
    description = "all or first")
  var replacementMode: String = "first"
  @Parameter(
    names = Array("-r", "--replacement"),
    description = "Replacement, default erases match string")
  var replacement: String = ""
  @Parameter(
    names = Array("-w", "--write-mode"),
    description = "Clean found patterns in file r|w, default is r - not write")
  var writeMode: String = "r"
}