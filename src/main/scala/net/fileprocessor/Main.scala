package net.fileprocessor

import java.io.{FileNotFoundException, IOException, File}
import java.util
import com.beust.jcommander.{JCommander, Parameter}
import scala.collection.JavaConversions._
import java.nio.file.{Paths, Files}
import java.nio.charset.StandardCharsets

import scala.util.control.Exception.Catch

object Main  {

  def loadPatternsFromFile(patternFilePaths: java.util.List[String]): java.util.List[String] = {
    var loadedPatters = new java.util.ArrayList[String]
    for (path <- patternFilePaths) {
      val pattern = scala.io.Source.fromFile(path).getLines().mkString
      loadedPatters.add(pattern)
    }
    loadedPatters
  }

  def replaceInFile(file: File, patternFilePaths: java.util.List[String], extensions: String,
                    replacement: String, replacementMode: String, writeMode: String) : String = {
    var contents = "None"
    val fileExtension = file.getName.replaceAll("^.*\\.([^.]+)$", "$1")
    val patterns = loadPatternsFromFile(patternFilePaths)
    if (fileExtension.endsWith(extensions)) {
      val fileOpened = scala.io.Source.fromFile(file.getAbsolutePath)
      try {
        if (fileOpened.nerrors == 0) {
          contents = fileOpened.getLines.mkString
          if (! contents.startsWith("[ERR_OPEN]")) {
            for (pattern <- patterns) {
              try {
                if (contents.contains(pattern)) {
                  println("found " + pattern.substring(12) + " in file: " + file.getAbsolutePath)
                  if (writeMode == "w") {
                    replacementMode match {
                      case "all" => contents = contents.replaceAll(pattern, replacement)
                      case _ => {
                        contents = contents.replaceFirst(pattern, replacement)
                      }
                    }
                  }
                }
              } catch {
                case ex: Exception => println(ex.getMessage)
              }
            }
          }
        }
      } catch {
        case ex: Exception => println("Error opening file for read")
      } finally {
        fileOpened.close()
      }
    }
    contents
  }

  def replaceInDirectory(directory: File, patternFilePaths: java.util.List[String], extensions: String,
                         replacement: String, replacementMode: String, writeMode: String) {
    val files = directory.listFiles()
    if (files != null) {
      for (file <- files) {
        if (file.isDirectory) {
          replaceInDirectory(file, patternFilePaths, extensions, replacement, replacementMode, writeMode)
        } else {
          if (file.canRead) {
            val contents = replaceInFile(file, patternFilePaths, extensions, replacement, replacementMode, writeMode)
            if (contents != "None") {
              Files.write(Paths.get(file.getAbsolutePath), contents.getBytes(StandardCharsets.UTF_8))
            }
          }
        }
      }
    }
  }

  def replaceInFiles(fileNames: java.util.List[String], patternFilePaths: java.util.List[String],
                     extensions: String, replacement: String, replacementMode: String, writeMode: String) {
    try {
      for (fileName <- fileNames) {
        val file = new File(fileName)
        if (file.exists() && file.isDirectory) {
          replaceInDirectory(file, patternFilePaths, extensions, replacement, replacementMode, writeMode)
        } else {
          replaceInFile(file, patternFilePaths, extensions, replacement, replacementMode, writeMode)
        }
      }
    } catch {
      case e: Exception => {
        println("\n" + e)
        println("\n" + e.getStackTrace + "\n")
      }
    } finally {

    }

  }
  def main(args: Array[String]) {
    new JCommander(Args, args.toArray: _*)
    for (action <- Args.actions) {
      if (action == "replace") {
        replaceInFiles(Args.files, Args.pattern, Args.extensions, Args.replacement, Args.replacementMode, Args.writeMode)
      } else {
        println("available commands: replace")
      }
    }
  }
}
